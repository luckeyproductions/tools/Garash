#include "../weaver.h"

#include <QDebug>

#include <QFileDialog>
#include <QDesktopServices>
#include <QDirIterator>

#include <QFormLayout>
#include <QVBoxLayout>
#include <QToolBar>

#include "projectwizard.h"


ProjectWizard::ProjectWizard(QWidget *parent, Project* project): QDialog(parent),
    project_{ (project ? std::make_shared<Project>(*project) : nullptr) },
    projectFolder_{},
    originalFileName_{},
    folderLabel_{ new QLabel{} },
    iconLabel_{ new QLabel{} },
    fileNameEdit_{ new QLineEdit{} },
    displayNameEdit_{ new QLineEdit{} },
    projectTree_{ new QTreeWidget{} },
    buttonBox_{ new QDialogButtonBox{ QDialogButtonBox::Ok | QDialogButtonBox::Cancel } }
{
    createInterface();

    connect(buttonBox_, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox_, SIGNAL(rejected()), this, SLOT(reject()));

    connect(fileNameEdit_, SIGNAL(textEdited(QString)), this, SLOT(handleFileNameEdited()));
    connect(fileNameEdit_, SIGNAL(editingFinished()),   this, SLOT(handleFileNameEdited()));
    connect(displayNameEdit_, SIGNAL(editingFinished()),   this, SLOT(handleDisplayNameEdited()));
    connect( projectTree_, SIGNAL(itemSelectionChanged()),
                     this, SLOT(handleSelectionChanged()));

    if (project_ == nullptr) // New project
    {
        project_ = std::make_shared<Project>();
    }
    else // Read project
    {
        project_ = std::shared_ptr<Project>(project);

        projectFolder_ = project->info_.location_;
        originalFileName_ = project->info_.fileName_;
        fileNameEdit_->setText(originalFileName_.data());
        displayNameEdit_->setText(project->info_.displayName_.data());
    }

    if (!projectFolder_.empty() || selectRootFolder())
    {
        folderLabel_->setText(projectFolder_.data());
        populateProjectTree();

        exec();
    }
}

void ProjectWizard::createInterface()
{
    setWindowTitle("Project Wizard");
    setWindowIcon(QIcon{ ":/Project" });

    QVBoxLayout* layout{ new QVBoxLayout{ this } };
    layout->setSizeConstraint(QLayout::SetFixedSize);

    QFormLayout* form{ new QFormLayout{} };
    form->setLabelAlignment(Qt::AlignRight);

    QHBoxLayout* formRow{ new QHBoxLayout{} };
    QVBoxLayout* formColumn{ new QVBoxLayout{} };

    iconLabel_->setPixmap(QPixmap{ ":/Default" }.scaled(0300, 0300));
    iconLabel_->setToolTip(DEFAULT_ICONTIP);
    formRow->addWidget(iconLabel_);

    QHBoxLayout* filenameLayout{ new QHBoxLayout{} };
    filenameLayout->addWidget(fileNameEdit_);
    QLabel* extensionLabel{ new QLabel{ ".lkp" } };
    extensionLabel->setEnabled(false);
    filenameLayout->addWidget(fileNameEdit_);
    filenameLayout->addWidget(extensionLabel);
    form->addRow("Filename:", filenameLayout);
    form->addRow("Display name:", displayNameEdit_);
    formColumn->addLayout(form);

    QHBoxLayout* treeRow{ new QHBoxLayout{} };
    treeRow->setSpacing(0);

    projectTree_->setSelectionMode(QAbstractItemView::SingleSelection);
    projectTree_->setHeaderHidden(true);
    projectTree_->setMinimumHeight(040);
    treeRow->addWidget(projectTree_);

    QToolBar* treeBar{ new QToolBar{} };
    treeBar->setIconSize(QSize{ 1, 1 } * 030);
    resourceAction_ = treeBar->addAction(QIcon{ ":/Icon" }, "Resource folder");
    resourceAction_->setEnabled(false);
    resourceAction_->setCheckable(true);
    iconAction_ = treeBar->addAction(QIcon{ ":/Project" }, "Project icon");
    iconAction_->setEnabled(false);
    iconAction_->setCheckable(true);
    treeBar->setOrientation(Qt::Vertical);
    treeRow->addWidget(treeBar);

    formColumn->addLayout(treeRow);
    formRow->addLayout(formColumn);

    folderLabel_->setEnabled(false);
    layout->addWidget(folderLabel_);
    layout->addLayout(formRow);
    layout->addWidget(buttonBox_);

    setModal(true);

    connect(resourceAction_, SIGNAL(triggered(bool)), this, SLOT(toggleResourceFolder(bool)));
    connect(iconAction_, SIGNAL(triggered(bool)), this, SLOT(setIcon(bool)));
}

bool ProjectWizard::selectRootFolder()
{
    std::string selectedFolder{ QFileDialog::getExistingDirectory(
                                        nullptr, tr("Select project root folder"),
                    (projectFolder_.empty()
                     ? QStandardPaths::writableLocation(QStandardPaths::HomeLocation)
                     : QString{ projectFolder_.data() })).toStdString() };

    if (selectedFolder.empty() | projectFolder_ == selectedFolder) // Cancelled / Unchanged
    {
        return false;
    }
    else
    {
        project_->info_.location_ = projectFolder_ = selectedFolder;

//        Context* c{ Weaver::context() };
//        Vector<String> projectFiles{};
//        c->GetSubsystem<FileSystem>()->ScanDir(projectFiles, project_->location_.data(), "*.lkp", SCAN_FILES, false);

//        for (const String& f: projectFiles)
//        {
//            if (project_->read(c->GetSubsystem<ResourceCache>()->GetResource<XMLFile>(AddTrailingSlash(project_->location_.data()) + f)))
//                break;
//        }

        return true;
    }
}

void ProjectWizard::populateProjectTree()
{
    const std::string rootFolderName{
        projectFolder_.substr(projectFolder_.find_last_of('/') + 1)
    };
    QTreeWidgetItem* folderItem{ new QTreeWidgetItem{ { rootFolderName.data(), "root" } } };
    folderItem->setToolTip(0, projectFolder_.data());

    projectTree_->addTopLevelItem(folderItem);
    growProjectTree(folderItem);

    projectTree_->sortItems(0, Qt::AscendingOrder);
    folderItem->setExpanded(true);
}

void ProjectWizard::growProjectTree(QTreeWidgetItem* parentItem)
{
    QString parentPath{ parentItem->toolTip(0) };

    if (parentPath.front() != '/')
        parentPath.prepend((projectFolder_ + '/').data());

    addImages(parentItem, parentPath);
    addSubFolders(parentItem, parentPath);
}

void ProjectWizard::addImages(QTreeWidgetItem* parentItem, const QString& parentPath)
{
    QDirIterator imageIterator(parentPath, { "*.png", "*.svg" }, QDir::Files);

    while (imageIterator.hasNext())
    {
        const std::string imageFile{ imageIterator.next().toStdString() };
        const std::string imageName{
            imageFile.substr(imageFile.find_last_of('/') + 1)
        };

        QTreeWidgetItem* imageItem{ new QTreeWidgetItem{ parentItem, { imageName.data(), "image" } } };
        imageItem->setToolTip(0, imageFile.substr(projectFolder_.size() + 1).data());
    }
}

void ProjectWizard::addSubFolders(QTreeWidgetItem* parentItem, const QString& parentPath)
{
    QDirIterator parentIterator(parentPath, QDir::Dirs);

    while (parentIterator.hasNext())
    {
        const std::string path{ parentIterator.next().toStdString() };
        const std::string folderName{ path.substr(path.find_last_of('/') + 1) };

        if (folderName.front() != '.')
        {
            QTreeWidgetItem* folderItem{ new QTreeWidgetItem{ { folderName.data(), "folder" } } };
            folderItem->setToolTip(0, path.substr(projectFolder_.size() + 1).data());

            parentItem->addChild(folderItem);
            growProjectTree(folderItem);
        }
    }
}

int ProjectWizard::exec()
{
    if (fileNameEdit_->text().isEmpty())
    {
        const std::string guessName{
            projectFolder_.substr(projectFolder_.find_last_of('/') + 1)
        };

        fileNameEdit_->setText(QString{ guessName.data() }.toLower());
        displayNameEdit_->setText(guessName.data());

        project_->info_.fileName_ = fileNameEdit_->text().toStdString();
        project_->info_.displayName_ = guessName;
    }

    return QDialog::exec();
}

void ProjectWizard::handleFileNameEdited()
{
    project_->info_.fileName_ = fileNameEdit_->text().toStdString();

    buttonBox_->button(QDialogButtonBox::Ok)->setEnabled(project_->info_.fileName_.size());
}

void ProjectWizard::handleDisplayNameEdited()
{
    project_->info_.displayName_ = displayNameEdit_->text().toStdString();
}

void ProjectWizard::handleSelectionChanged()
{
    QTreeWidgetItem* item{ projectTree_->currentItem() };

    if (item)
    {
        const char typeChar{ item->text(1).front().toLatin1() };

        if (typeChar != 'i')
        {
            iconAction_->setEnabled(false);
            iconAction_->setChecked(false);

            if (typeChar == 'r') // Root folder
            {
                resourceAction_->setEnabled(false);
                resourceAction_->setChecked(false);
            }
            else if (typeChar == 'f')
            {
                const std::string itemPath{ projectTree_->currentItem()->toolTip(0).toStdString() };

                resourceAction_->setEnabled(true);
                resourceAction_->setChecked(project_->usesFolder(itemPath));
            }
        }
        else
        {
            const std::string iconPath{ item->toolTip(0).toStdString() };

            resourceAction_->setEnabled(false);
            resourceAction_->setChecked(false);

            iconAction_->setEnabled(true);
            iconAction_->setChecked(iconPath == project_->info_.icon_);
        }
    }
    else
    {
        resourceAction_->setEnabled(false);
        resourceAction_->setChecked(false);
        iconAction_->setEnabled(false);
        iconAction_->setChecked(false);
    }
}

void ProjectWizard::toggleResourceFolder(bool checked)
{
    const std::string itemPath{ projectTree_->currentItem()->toolTip(0).toStdString() };

    if (checked)
        project_->addFolder(itemPath);
    else
        project_->removeFolder(itemPath);
}

void ProjectWizard::setIcon(bool checked)
{
    if (checked)
    {
        QTreeWidgetItem* item{ projectTree_->currentItem() };

        if (item)
        {
            const std::string iconPath{ projectTree_->currentItem()->toolTip(0).toStdString() };
            const std::string fullPath{ projectFolder_ +'/'+ iconPath };

            QPixmap icon{ QString{ fullPath.data() } };

            if (!icon.isNull())
            {
                iconLabel_->setPixmap(icon.scaled(0300, 0300));
                project_->info_.icon_ = iconPath;

                return;
            }
        }
    }

    project_->info_.icon_.clear();
    iconLabel_->setPixmap(QPixmap{ ":/Default" }.scaled(0300, 0300));
}
