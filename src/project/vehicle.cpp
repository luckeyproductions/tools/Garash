/* Garash
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "../qocoon.h"
#include "../garashevents.h"

#include "vehicle.h"

Vehicle* Vehicle::active_{ nullptr };

Vehicle::Vehicle(Context* context): Serializable(context),
    scene_{ nullptr },
    jibNode_{ nullptr },
    rootNode_{ nullptr },
    steeringNode_{ nullptr },
    vehicleModel_{ nullptr },
    vehicleBody_{ nullptr },
    raycastVehicle_{ nullptr },
    vehicleHull_{ nullptr },
    nodeId_{ M_MAX_UNSIGNED },
    name_{},
    steeringNodeName_{},
    scale_{ 1.f },
    previousPosition_{},
    steering_{},
    brake_{},
    handbrake_{},
    throttle_{}
{
    SubscribeToEvent(E_SCENERESTORED, DRY_HANDLER(Vehicle, HandleSceneRestored));
    SubscribeToEvent(E_POSTRENDERUPDATE, DRY_HANDLER(Vehicle, HandlePostRenderUpdate));
    SubscribeToEvent(E_UPDATE, DRY_HANDLER(Vehicle, HandleUpdate));
}

void Vehicle::Init(Scene* scene)
{
    scene_ = scene;
    jibNode_ = scene_->GetChild("Jib");

    rootNode_ = scene_->CreateChild("Vehicle");
    nodeId_ = rootNode_->GetID();
    vehicleBody_ = rootNode_->CreateComponent<RigidBody>();
    vehicleBody_->SetCollisionLayer(1);
    vehicleBody_->SetFriction(.25f);
    vehicleModel_ = rootNode_->CreateComponent<AnimatedModel>();
    vehicleModel_->SetCastShadows(true);

    raycastVehicle_ = rootNode_->CreateComponent<RaycastVehicle>();
    raycastVehicle_->Init();
}

void Vehicle::SetName(const String& name)
{
    if (name == name_)
        return;

    name_ = name;

    if (rootNode_)
        rootNode_->SetName(name_);
}

void Vehicle::SetModel(Model* model)
{
    vehicleModel_->SetModel(model);
    float scale{ 1.f };

    if (vehicleModel_->GetModel())
    {
        vehicleModel_->ApplyMaterialList();
        if (Material* mat{ vehicleModel_->GetMaterial(0) })
            mat->SetShaderParameter("MatDiffColor", Color::CHARTREUSE);

        const BoundingBox vehicleBounds{ vehicleModel_->GetBoundingBox() };
        const Vector3 vehicleSize{ VectorAbs(vehicleBounds.Size()) };
        scale = Max(vehicleSize.Data(), vehicleSize.Data() + 3);

        const float margin{ .01f * scale };
        const Vector3 shrink{ 2.f * margin * Vector3::ONE };
        const Vector3 hullSize{ vehicleBounds.Size() * Vector3{ 1.f, .5f, 1.f } };

        vehicleHull_ = rootNode_->CreateComponent<CollisionShape>();
        vehicleHull_->SetBox(hullSize - shrink, vehicleBounds.Center());
        vehicleHull_->SetMargin(margin);

        const float volume{ vehicleBounds.Size().x_ * vehicleBounds.Size().y_ * vehicleBounds.Size().z_ };
        vehicleBody_->SetMass(23.f * volume / scale);
        vehicleBody_->UpdateMass();

        AutoWheel();
    }

    scale_ = scale;
}

void Vehicle::AutoWheel()
{
    PODVector<Node*> wheelNodes{};
    PODVector<Node*> nodes{};
    rootNode_->GetChildren(nodes, true);

    for (Node* n: nodes)
    {
        const String& nodeName{ n->GetName() };

        if (nodeName.Contains("steer", false) || nodeName.Contains("fork", false))
            steeringNode_ = n;
        else if (nodeName.Contains("wheel", false))
            wheelNodes.Push(n);
    }

    if (steeringNode_)
        steeringNodeName_ = steeringNode_->GetName();
    else
        steeringNodeName_.Clear();

    const bool bike{ wheelNodes.Size() < 4 };
    const float maxSuspensionForce{ vehicleBody_->GetMass() * 55.f / wheelNodes.Size() };

    for (Node* w: wheelNodes)
    {
        const Vector3 rootPos{ rootNode_->WorldToLocal(w->GetWorldPosition()) };
        const float radius{ rootPos.y_ };
        const bool front{ rootPos.z_ > 0.f };

//        const float offCenter{ rootPos.x_ / vehicleHull_->GetSize().x_ };

        const Vector3 direction{ (Vector3::DOWN + bike * !front * .5f * Vector3::BACK /* + offCenter * Vector3::RIGHT*/).Normalized() };
        const Vector3 axle{ Vector3::BACK.CrossProduct(direction).Normalized() };
        const float restLength{ radius };
        const Vector3 offset{ rootNode_->GetWorldPosition() - vehicleBody_->GetCenterOfMass() };
        w->Translate( -.75f * restLength * direction + offset, TS_WORLD);
        w->Rotate({ 180.f, Vector3::RIGHT });
        const int wId{ raycastVehicle_->GetNumWheels() };
        raycastVehicle_->AddWheel(w, direction, axle, restLength, radius, front);
        w->Rotate({ 180.f, Vector3::RIGHT });
        raycastVehicle_->SetWheelSuspensionStiffness(wId, 200.f);
        raycastVehicle_->SetWheelMaxSuspensionForce(wId, maxSuspensionForce);
        raycastVehicle_->SetWheelDampingCompression(wId, restLength * 2.f);
        raycastVehicle_->SetWheelDampingRelaxation(wId, 5.f);
        raycastVehicle_->SetWheelFrictionSlip(wId, 5.f);
        raycastVehicle_->SetWheelSkidInfo(wId, .1f);
        raycastVehicle_->SetWheelRollInfluence(wId, .125f);
        raycastVehicle_->SetMaxSideSlipSpeed(scale_ * .1f);
    }

    raycastVehicle_->ResetWheels();
}

void Vehicle::HandleUpdate(StringHash /*eventType*/, VariantMap& eventData)
{
    if (rootNode_->GetScene() != scene_ || !scene_->IsUpdateEnabled())
        return;

    const bool parked{ active_ != this };
    const float timeStep{ eventData[Update::P_TIMESTEP].GetFloat() };
    const Vector3 move{ (parked ? Vector3{ 0.f, 0.f, -1.f } : GetSubsystem<Qocoon>()->getMove()) };

    UpdateSteering(move.x_, timeStep);
    const bool braking{ UpdateBrake({ move.z_, move.y_ }, timeStep) };

    const float targetThrottle{ !braking * move.y_ * 55.f * (1.f - 1/3.f * (move.y_ < 0.f)) };
    if (throttle_ != targetThrottle)
        throttle_ = Lerp(throttle_, targetThrottle, PowN(4.f, 1 + (targetThrottle == 0.f)) * timeStep);

    for (int w{ 0 }; w < raycastVehicle_->GetNumWheels(); ++w)
    {
        const bool front{ raycastVehicle_->IsFrontWheel(w) };

        if (front)
        {
            raycastVehicle_->SetSteeringValue(w, steering_);
            raycastVehicle_->SetBrake(w, brake_);
        }
        else
        {
            raycastVehicle_->SetEngineForce(w, targetThrottle);
            raycastVehicle_->SetBrake(w, Max(brake_, handbrake_));
        }
    }

    if (steeringNode_)
    {
        Bone* bone{ vehicleModel_->GetSkeleton().GetBone(steeringNode_->GetName()) };
        const Quaternion boneStartRot{ bone->initialRotation_ };
        const Vector3 steeringAxis{ boneStartRot * Vector3::DOWN };

        steeringNode_->SetRotation(Quaternion{ M_RADTODEG * steering_, steeringAxis } * boneStartRot);
    }


    const Vector3 pos{ rootNode_->GetWorldPosition() };

    if (!parked)
        jibNode_->GetComponent<Jib>()->translate(pos - previousPosition_);

    previousPosition_ = pos;
}

void Vehicle::UpdateSteering(float input, float timeStep)
{
    constexpr float maxSteering{ M_TAU / 6.f };
    const float targetSteering{ input * maxSteering };

    if (steering_ != targetSteering)
        steering_ = Lerp(steering_, targetSteering, PowN(4.f, 1 + (input == 0.f)) * timeStep);
}

bool Vehicle::UpdateBrake(const Vector2& input, float timeStep)
{
    const float brakeForce{ Sqrt(vehicleBody_->GetMass()) };

    const float forwardness{ vehicleBody_->GetLinearVelocity().DotProduct(rootNode_->GetWorldDirection()) };
    const bool braking{ input.y_ * forwardness < -.5f };
    const float targetBrake{ .5f * brakeForce * braking };
    if (brake_ != targetBrake)
        brake_ = Lerp(brake_, targetBrake, PowN(2.f, 1 + !braking) * timeStep);

    const bool handbraking{ input.x_ < 0.f };
    const float targetHandbrake{ brakeForce * handbraking };
    if (handbrake_ != targetHandbrake)
        handbrake_ = Lerp(handbrake_, targetHandbrake, PowN(2.f, 1 + 2 * handbraking) * timeStep);

    return braking || handbraking;
}

void Vehicle::HandlePostRenderUpdate(StringHash /*eventType*/, VariantMap& /*eventData*/)
{
    if (rootNode_->GetScene() != scene_)
        return;

    DebugRenderer* debug{ scene_->GetComponent<DebugRenderer>() };
    raycastVehicle_->DrawDebugGeometry(debug, false);
}

void Vehicle::HandleSceneRestored(StringHash /*eventType*/, VariantMap& eventData)
{
    if (!scene_ || scene_ != static_cast<Scene*>(eventData[SceneRestored::P_SCENE].GetPtr()))
        return;

    rootNode_ = scene_->GetNode(nodeId_);
    vehicleModel_ = rootNode_->GetComponent<AnimatedModel>();
    vehicleBody_ = rootNode_->GetComponent<RigidBody>();
    raycastVehicle_ = rootNode_->GetComponent<RaycastVehicle>();
    raycastVehicle_->ResetWheels();
    vehicleHull_ = rootNode_->GetComponent<CollisionShape>(true);

    if (!steeringNodeName_.IsEmpty())
        steeringNode_ = rootNode_->GetChild(steeringNodeName_, true);

    steering_ = brake_ = handbrake_ = throttle_ = 0.f;

    jibNode_ = scene_->GetChild("Jib");
    previousPosition_ = rootNode_->GetWorldPosition();
}
