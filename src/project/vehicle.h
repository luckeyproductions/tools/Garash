/* Garash
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef VEHICLE_H
#define VEHICLE_H

#include "../dry.h"

class Vehicle: public Serializable
{
    DRY_OBJECT(Vehicle, Serializable);

public:
    static Vehicle* GetActive() { return active_; }

    Vehicle(Context* context);

    void Init(Scene* scene);
    void SetName(const String& name);
    void SetModel(Model* model);
    void SetActive() { active_ = this; }

    float GetScale() const { return scale_; }
    float GetHalfHeight() const
    {
        if (vehicleModel_)
            return vehicleModel_->GetBoundingBox().HalfSize().y_;
        else
            return 0.f;
    }

    Node* GetNode() const { return rootNode_; }
    AnimatedModel* GetAnimatedModel() const { return vehicleModel_; }
    Model* GetModel() const { return vehicleModel_->GetModel(); }

    void ResetWheels()
    {
        if (raycastVehicle_)
            raycastVehicle_->ResetWheels();
    }

private:
    static Vehicle* active_;

    void AutoWheel();

    void UpdateSteering(float input, float timeStep);
    bool UpdateBrake(const Vector2& input, float timeStep);

    Scene* scene_;
    Node* jibNode_;

    Node* rootNode_;
    Node* steeringNode_;
    AnimatedModel* vehicleModel_;
    RigidBody* vehicleBody_;
    RaycastVehicle* raycastVehicle_;
    CollisionShape* vehicleHull_;

    unsigned nodeId_;
    String name_;
    String steeringNodeName_;
    float scale_;

    Vector3 previousPosition_;
    float steering_;
    float brake_;
    float handbrake_;
    float throttle_;

    void HandlePostRenderUpdate(StringHash eventType, VariantMap& eventData);
    void HandleUpdate(StringHash eventType, VariantMap& eventData);
    void HandleSceneRestored(StringHash eventType, VariantMap& eventData);
};

#endif // VEHICLE_H
