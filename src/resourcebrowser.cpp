/* ManaWarg
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QDrag>
#include <QDragMoveEvent>
#include <QStyle>
#include <QHeaderView>
#include <QMenu>
#include <QSettings>
#include <QVBoxLayout>

#include "qocoon.h"
#include "weaver.h"
#include "project/project.h"

#include "resourcebrowser.h"

ResourceBrowser::ResourceBrowser(Context* context, QWidget* parent): DryWidget(context, parent),
    browser_{ new QWidget{ this } },
    treeViewWidget_{ new QWidget{ this } },
    treeWidget_{ nullptr },
    iconViewWidget_{ new QWidget{ this } },
    iconWidget_{ nullptr },
    splitter_{ new FlipSplitter{ this, 1 } },
    resourcePreview_{ new ResourcePreview{ context, this } }
{
    setObjectName("Resource Browser");

    QVBoxLayout* mainLayout{ new QVBoxLayout{ this } };
    mainLayout->setMargin(0);

    QVBoxLayout* browserLayout{ new QVBoxLayout{} };
    browserLayout->setMargin(2);
    browser_->setLayout(browserLayout);

    createTreeViewWidget();
    createIconViewWidget();

    splitter_->setObjectName("resourcebrowser/splitter");
    splitter_->addWidget(browser_);
    splitter_->addWidget(resourcePreview_);
    splitter_->setStretchFactors(Qt::Horizontal, 1, 0);
    splitter_->setStretchFactors(Qt::Vertical,   3, 1);

    mainLayout->addWidget(splitter_);
    setLayout(mainLayout);

    QAction* activateAction{ new QAction{ "Activate", this } };
    activateAction->setShortcuts({ QKeySequence{ Qt::Key_Enter }, QKeySequence{ Qt::Key_Return } });
    connect(activateAction, SIGNAL(triggered(bool)), resourcePreview_, SLOT(playSound()));
    addAction(activateAction);

    refreshTreeView();

    Qocoon* qocoon{ GetSubsystem<Qocoon>() };
    connect(qocoon, SIGNAL(resourceFoldersChanged()), SLOT(refreshTreeView()));
    connect(qocoon, SIGNAL(currentProjectChanged()), SLOT(refreshTreeView()));

}

ResourceBrowser::~ResourceBrowser()
{
    QSettings settings{};
    settings.setValue(splitter_->objectName() + "/geometry", splitter_->saveState());
}

void ResourceBrowser::showEvent(QShowEvent*)
{
//    if (firstShow_)
//    {
//        const QSettings settings{};
//        const QByteArray stateBytes{ settings.value(splitter_->objectName() + "/geometry").toByteArray() };

//        if (!stateBytes.isNull())
//        {
//            splitter_->restoreState(stateBytes);
//        }

//        firstShow_ = false;
//    }
//    else
//    {
//        bool horizontal{ splitter_->orientation() == Qt::Horizontal };
//        int max{ horizontal ? height() : width() };

//        if (!splitter_->widget(0)->visibleRegion().isEmpty()
//            && !splitter_->widget(1)->visibleRegion().isEmpty())
//        {
//            int sizesSum{ splitter_->sizes()[0] + splitter_->sizes()[1] };

//            splitter_->setSizes({ sizesSum - max, max });
//        }
//    }
}

void ResourceBrowser::showBrowserMenu(const QPoint& pos)
{
    auto project{ GetSubsystem<Qocoon>()->project() };

    if (!project)
        return;

    QMenu browserMenu{};
    QTreeWidgetItem* item{ treeWidget_->itemAt(pos) };

    if (item && !item->parent())
    {
//        if (browserMenu.addAction(QIcon(":/Delete"), "Dismiss resource folder")
//            == browserMenu.exec(mapToGlobal(pos)))
//        {
//            String path{ toString(item->data(0, FileName).toString()) };
//            project->removeResourceFolder(path);
//        }
    }
}

void ResourceBrowser::createTreeViewWidget()
{
    QVBoxLayout* treeViewLayout{ new QVBoxLayout{ treeViewWidget_ } };
    treeViewLayout->setMargin(0);
    treeWidget_ = new QTreeWidget{};
    treeWidget_->setMinimumSize(64, 64);
    treeWidget_->setTextElideMode(Qt::ElideNone);
    treeWidget_->setHeaderHidden(true);
//    treeWidget_->setHeaderLabels({"", "Type"});
//    treeWidget_->setColumnCount(2); //For some reason breaks drag 'n drop
//    treeWidget_->setColumnWidth(0, 256);
    treeWidget_->header()->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
    treeWidget_->header()->setSectionResizeMode(0, QHeaderView::Stretch);
    treeWidget_->header()->setStretchLastSection(false);
    treeWidget_->setDragEnabled(true);

    treeViewLayout->addWidget(treeWidget_);
    treeViewWidget_->setLayout(treeViewLayout);
    treeWidget_->setContextMenuPolicy(Qt::CustomContextMenu);

    connect(treeWidget_, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(showBrowserMenu(const QPoint&)));
    connect(treeWidget_, SIGNAL(itemExpanded(QTreeWidgetItem*)), SLOT(updateFolderIcon(QTreeWidgetItem*)));
    connect(treeWidget_, SIGNAL(itemCollapsed(QTreeWidgetItem*)), SLOT(updateFolderIcon(QTreeWidgetItem*)));
    connect(treeWidget_, SIGNAL(currentItemChanged(QTreeWidgetItem*, QTreeWidgetItem*)),
            resourcePreview_, SLOT(changeResource(QTreeWidgetItem*)));

    browser_->layout()->addWidget(treeViewWidget_);
}

void ResourceBrowser::createIconViewWidget()
{
    QVBoxLayout* iconViewLayout{ new QVBoxLayout{ iconViewWidget_ } };
    iconViewLayout->setMargin(0);
    iconWidget_ = new QListWidget{};

    iconViewLayout->addWidget(iconWidget_);
    iconViewWidget_->setLayout(iconViewLayout);
    browser_->layout()->addWidget(iconViewWidget_);

    iconViewWidget_->hide();
}

void ResourceBrowser::refreshTreeView()
{
    treeWidget_->clear();

    auto project{ GetSubsystem<Qocoon>()->project() };

    if (!project)
        return;

    QList<QTreeWidgetItem*> resourceFolderItems{};

    auto folders{ project->info_.resourceFolders_ };

    for (auto path: folders)
    {
        if (path.empty())
            continue;

        const QString displayName{ toQString(GetFileName(RemoveTrailingSlash({ path.data() }))) };
        QString fullPath{ path.data() };

        if (!IsAbsolutePath(toString(fullPath)))
            fullPath = toQString(AddTrailingSlash({ project->info_.location_.data() })) + fullPath;

        QTreeWidgetItem* resourceFolderItem{ new QTreeWidgetItem{} };

        resourceFolderItem->setText(0, displayName);
        resourceFolderItem->setIcon(0, QIcon{ ":/Folder" });
        resourceFolderItem->setData(0, FileName, QVariant{ fullPath });
        resourceFolderItem->setToolTip(0, fullPath);
        resourceFolderItem->setFlags(resourceFolderItem->flags() ^ Qt::ItemIsDragEnabled );

        buildTree(resourceFolderItem);

        resourceFolderItems.append(resourceFolderItem);
    }

    QTreeWidgetItem* resourceFolderItem{ new QTreeWidgetItem{} };
    treeWidget_->addTopLevelItems(resourceFolderItems);
}

void ResourceBrowser::buildTree(QTreeWidgetItem* treeItem)
{
    FileSystem* fs{ GetSubsystem<FileSystem>() };
    const String fullPath{ toString(treeItem->data(0, FileName).toString()) };

    Vector<String> scanResults{};
    fs->ScanDir(scanResults, fullPath, "*.*", SCAN_DIRS, false);
    Sort(scanResults.Begin(), scanResults.End());

    for (String subFolder: scanResults)
    {
        if (subFolder.Front() == '.')
            continue;

        QString qSub{ toQString(subFolder) };
        QTreeWidgetItem* subFolderItem{ new QTreeWidgetItem{} };

        subFolderItem->setText(0, qSub);
        subFolderItem->setIcon(0, QIcon(":/Folder"));
        subFolderItem->setData(0, FileName, QVariant(toQString(AddTrailingSlash(AddTrailingSlash(fullPath) + subFolder))));
        subFolderItem->setFlags(subFolderItem->flags() ^ Qt::ItemIsDragEnabled );
        subFolderItem->setToolTip(0, toQString(AddTrailingSlash(fullPath) + subFolder));

        buildTree(subFolderItem);
        treeItem->addChild(subFolderItem);
    }

    fs->ScanDir(scanResults, fullPath, "*.*", SCAN_FILES, false);
    Sort(scanResults.Begin(), scanResults.End());

    for (const String& fileName: scanResults)
    {
        QTreeWidgetItem* fileItem{ new QTreeWidgetItem{} };
        const String fullFileName{ AddTrailingSlash(fullPath) + fileName };
        const String extension{ GetExtension(fileName) };
        QString iconName{ ":/New" };

        fileItem->setText(0, toQString(fileName));
        fileItem->setToolTip(0, toQString(fullFileName));

        if (extension == ".png" || extension == ".jpg" || extension == ".dds")
        {
            fileItem->setData(0, DryTypeHash, QVariant(Texture2D::GetTypeStatic().ToHash()));
            fileItem->setText(1, "Image");
            iconName = ":/Image";
        }
        else if (extension == ".ogg" || extension == ".wav")
        {
            fileItem->setData(0, DryTypeHash, QVariant{ Sound::GetTypeStatic().ToHash() });
            fileItem->setText(1, "Sound");
            iconName = ":/Sound";
        }
        else if (extension == ".mdl")
        {
            fileItem->setData(0, DryTypeHash, QVariant{ Model::GetTypeStatic().ToHash() });
            fileItem->setText(1, "Model");
            iconName = ":/Model";
        }
        else if (extension == ".ani")
        {
            fileItem->setData(0, DryTypeHash, QVariant{ Animation::GetTypeStatic().ToHash() });
            fileItem->setText(1, "Animation");
            iconName = ":/Animation";
        }
        else if (extension == ".as")
        {
            fileItem->setData(0, DryTypeHash, QVariant{ ScriptFile::GetTypeStatic().ToHash() });
            fileItem->setText(1, "Script");
        }
        else if (extension == ".xml")
        {
            SharedPtr<XMLFile> xmlFile{ GetSubsystem<ResourceCache>()->GetTempResource<XMLFile>(fullFileName) };

            if (xmlFile)
            {
                String rootName{ xmlFile->GetRoot().GetName() };

                if (rootName == "material")
                {
                    fileItem->setData(0, DryTypeHash, QVariant{ Material::GetTypeStatic().ToHash() });
                    fileItem->setText(1, "Material");
                    iconName = ":/Material";
                }
                else if (rootName == "technique")
                {
                    fileItem->setData(0, DryTypeHash, QVariant{ Technique::GetTypeStatic().ToHash() });
                    fileItem->setText(1, "Technique");
                }
                else if (rootName == "scene")
                {
                    fileItem->setData(0, DryTypeHash, QVariant{ Scene::GetTypeStatic().ToHash() });
                    fileItem->setText(1, "Scene");
                }
                else if (rootName == "node")
                {
                    fileItem->setData(0, DryTypeHash, QVariant{ Node::GetTypeStatic().ToHash() });
                    fileItem->setText(1, "Prefab");
                }
                else if (rootName == "particleeffect")
                {
                    fileItem->setData(0, DryTypeHash, QVariant{ ParticleEffect::GetTypeStatic().ToHash() });
                    fileItem->setText(1, "Particle Effect");
                    iconName = ":/Particles";
                }
                else
                {
                    fileItem->setData(0, DryTypeHash, QVariant{ XMLFile::GetTypeStatic().ToHash() });
                    fileItem->setText(1, "XML File");
                }
            }
        }

        fileItem->setData(0, FileName, QVariant{ toQString(fullFileName) });
        fileItem->setIcon(0, QIcon{ iconName });

        treeItem->addChild(fileItem);
    }
}

void ResourceBrowser::updateFolderIcon(QTreeWidgetItem* item)
{
    item->setIcon(0, QIcon{ ":/" + QString{ item->isExpanded() ? "Open" : "Folder" } });
}

///

ResourcePreview::ResourcePreview(Context* context, QWidget* parent): QWidget(parent), Object(context),
    view3d_{ new View3D{ context } },
    noPreviewLabel_{ new QLabel{ "No preview" } },
    soundButton_{ new QPushButton{ QIcon{ ":/Sound" }, "Play" } },
    soundTimer_{ new QTimer{ this } },
    resource_{ nullptr },
    previewModel_{ nullptr },
    previewSoundSource_{ nullptr },
    previewType_{ RT_None }
{
    noPreviewLabel_->setEnabled(false);
    soundButton_->setCheckable(true);
    
    QVBoxLayout* mainLayout{ new QVBoxLayout{ this } };
    mainLayout->setMargin(1);
    mainLayout->addWidget(noPreviewLabel_);
    mainLayout->addWidget(view3d_);
    mainLayout->addWidget(soundButton_);
    mainLayout->setAlignment(noPreviewLabel_, Qt::AlignCenter);

    createPreviewScene();

    view3d_->setVisible(false);
    soundButton_->setVisible(false);

    connect(soundButton_, SIGNAL(toggled(bool)), this, SLOT(soundButtonToggled(bool)));
    connect(soundTimer_, SIGNAL(timeout()), this, SLOT(soundFinished()));

    setLayout(mainLayout);
}

void ResourcePreview::createPreviewScene()
{
    Scene* previewScene{ new Scene{ context_ } };
    previewScene->AddTag("Preview");
    previewScene->CreateComponent<Octree>();
    previewScene->CreateComponent<Zone>();

    //Light
    Node* lightNode{ previewScene->CreateChild("Light") };
    lightNode->SetPosition(Vector3{ 2.0f, 3.0f, -1.0f });
    lightNode->CreateComponent<Light>()/*->SetBrightness(1.23f)*/;

    //Preview model
    Node* modelNode{ previewScene->CreateChild("Model") };
    modelNode->SetTemporary(true);
    previewModel_ = modelNode->CreateComponent<AnimatedModel>();
    modelNode->CreateComponent<AnimationController>();

    //Sound
    previewSoundSource_ = previewScene->CreateComponent<SoundSource>();

    view3d_->setScene(previewScene);
//    lightNode->SetParent(view3d_->jib());
}

void ResourcePreview::changeResource(QTreeWidgetItem* item)
{
    previewType_ = RT_None;

    if (item)
    {
        QString fileName{ item->data(0, FileName).toString() };
        unsigned typeHash{ item->data(0, DryTypeHash).toUInt() };

        if (fileName.endsWith('/'))
            return;

        if (typeHash != 0 && !fileName.isEmpty())
        {
            if (Material::GetTypeStatic().ToHash() == typeHash)
                setMaterial(fileName);
            else if (Model::GetTypeStatic().ToHash() == typeHash)
                setModel(fileName);
            else if (Sound::GetTypeStatic().ToHash() == typeHash)
                setSound(fileName);
        }
    }

    view3d_->setVisible(previewType_ == RT_Material || previewType_ == RT_Model);
    soundButton_->setVisible(previewType_ == RT_Sound);
    soundButton_->setChecked(false);
    noPreviewLabel_->setVisible(previewType_ == RT_None);
}

void ResourcePreview::setMaterial(const QString& fileName)
{
    previewType_ = RT_Material;

    previewModel_->SetModel(GetSubsystem<ResourceCache>()->GetTempResource<Model>("Models/Triacontahedron.mdl"));
    previewModel_->SetMaterial(GetSubsystem<ResourceCache>()->GetTempResource<Material>(toString(fileName)));

    Node* previewNode{ previewModel_->GetNode() };
    previewNode->SetScale(1.0f);
    previewNode->SetPosition(Vector3::ZERO);
    previewNode->SetTags({ TAG_DEFAULT });

    view3d_->jib()->SetFov(36.0f);

    view3d_->updateView();
}

void ResourcePreview::setModel(const QString& fileName)
{
    previewType_ = RT_Model;

    SharedPtr<Model> model{ GetSubsystem<ResourceCache>()->GetTempResource<Model>(toString(fileName)) };
    previewModel_->SetModel(model);

    if (!model)
        changeResource(nullptr);

    previewModel_->SetMaterial(GetSubsystem<ResourceCache>()->GetTempResource<Material>("Materials/None.xml"));

    Node* previewNode{ previewModel_->GetNode() };

    previewNode->SetScale(M_1_SQRT3 / previewModel_->GetBoundingBox().HalfSize().Length());
    previewNode->SetPosition(-previewModel_->GetBoundingBox().Center() * previewNode->GetScale().x_);
    previewNode->SetRotation(Quaternion::IDENTITY);
    previewNode->SetTags({});

    view3d_->updateView();
}

void ResourcePreview::setSound(const QString& fileName)
{
    previewType_ = RT_Sound;

    soundButton_->setProperty("fileName", fileName);
}

void ResourcePreview::soundButtonToggled(bool checked)
{
    soundTimer_->stop();

    if (previewSoundSource_->IsPlaying())
        previewSoundSource_->Stop();

    if (checked)
    {
        ResourceCache* cache{ GetSubsystem<ResourceCache>() };
        SharedPtr<Sound> sound{ cache->GetTempResource<Sound>(toString(soundButton_->property("fileName").toString())) };

        previewSoundSource_->Play(sound, sound->GetFrequency());
        soundTimer_->start(sound->GetLength() * 1000);
    }
}

void ResourcePreview::soundFinished()
{
    soundButton_->toggle();
}

void ResourcePreview::playSound()
{
    if (previewType_ == RT_Sound)
    {
        if (soundButton_->isChecked())
            soundButton_->toggle();

        soundButton_->toggle();
    }
}
