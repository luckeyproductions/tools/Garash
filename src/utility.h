#ifndef UTILITY_H
#define UTILITY_H

#define QD(s) String{ s.toLatin1().data() }
#define DQ(s) QString{ s.CString() }

#endif // UTILITY_H
