/* Garash
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include <QDebug>

#include <QMenuBar>
#include <QToolBar>
#include <QStatusBar>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QLabel>
#include <QSettings>
#include <QFileDialog>
#include <QDesktopServices>
#include <QDesktopWidget>
#include <QUrl>
#include <QAction>
#include <QResizeEvent>
#include <QVBoxLayout>
#include <QUndoStack>

#include "view3d.h"
#include "project/projectwizard.h"
#include "project/vehicle.h"
#include "garage.h"
#include "weaver.h"
#include "utility.h"

#include "qocoon.h"

Qocoon::Qocoon(Context* context): QMainWindow(), Object(context),
    project_{ nullptr },
    garageNodeId_{ M_MAX_UNSIGNED },
    garage_{ nullptr },
    vehicles_{},
    projectDependentActions_{},
    mainToolBar_{ nullptr },
    undoView_{ new QUndoView{ this } },
    view3d_{ new View3D{ context, this } },
    skeletonWidget_{ new SkeletonWidget{ this } },
    boneWidget_{ new BoneWidget{ this } },
    resourceBrowser_{ nullptr },
    recentMenu_{ nullptr },
    actionTest_{ nullptr },
    actionUndo_{ nullptr },
    actionRedo_{ nullptr },
    actionFullView_{ new QAction{ QIcon{ ":/FullView" }, "Fullscreen View", this } },
    hiddenDocks_{},
    pressedKeys_{}
{
    context_->RegisterSubsystem(this);
    setWindowIcon(QIcon{ ":/Icon" });
    setCentralWidget(view3d_);
    setTabPosition(Qt::LeftDockWidgetArea, QTabWidget::North);

    undoView_->setEmptyLabel("Initial");
    undoView_->setMinimumHeight(040);
    undoView_->setStack(new QUndoStack{ this });
    QDockWidget* undoDockWidget{ new QDockWidget{ "History", this } };
    undoDockWidget->setObjectName("History");
    undoDockWidget->setWidget(undoView_);
    addDockWidget(Qt::LeftDockWidgetArea, undoDockWidget);

    context_->RegisterSubsystem(skeletonWidget_);
    createDockWidget(skeletonWidget_, Qt::LeftDockWidgetArea);
    createDockWidget(boneWidget_, Qt::LeftDockWidgetArea);
    connect(skeletonWidget_, SIGNAL(currentBoneChanged(Node*)), boneWidget_, SLOT(setBone(Node*)));
    connect(skeletonWidget_, SIGNAL(selectedBonesChanged()), view3d_, SLOT(refreshGarnish()));

    resourceBrowser_ = new ResourceBrowser{ context, this };
    createDockWidget(resourceBrowser_, Qt::BottomDockWidgetArea);

    setCorner(Qt::BottomRightCorner, Qt::BottomDockWidgetArea);
    setCorner(Qt::BottomLeftCorner,  Qt::LeftDockWidgetArea);

    actionFullView_->setCheckable(true);
    actionFullView_->setShortcut(QKeySequence{ "F11" });
    addAction(actionFullView_);
    connect(actionFullView_, SIGNAL(triggered(bool)), this, SLOT(toggleFullView(bool)));

    connect(this, SIGNAL(currentProjectChanged()), SLOT(updateAvailableActions()));
    connect(undoView_->selectionModel(), SIGNAL(currentRowChanged(QModelIndex, QModelIndex)),
            view3d_, SLOT(updateView()));
    connect(undoView_->selectionModel(), SIGNAL(currentRowChanged(QModelIndex, QModelIndex)),
            view3d_, SLOT(updateView()));

    createToolBar();
    createMenuBar();
    setStatusBar(new QStatusBar{ this });
    loadSettings();

    context_->RegisterFactory<Vehicle>();
    context_->RegisterFactory<Garage>();
    createScene();
    loadModel("Models/CargoHonti.mdl");
    statusBar()->showMessage("Welcome!");

    show();
}

Qocoon::~Qocoon()
{
    if (actionFullView_->isChecked())
        actionFullView_->trigger();

    QSettings settings{};
    settings.setValue("geometry", saveGeometry());
    settings.setValue("state", saveState());
}

void Qocoon::loadSettings()
{
    QSettings settings{};

    restoreGeometry(settings.value("geometry").toByteArray());
    restoreState(settings.value("state").toByteArray());
}

void Qocoon::createToolBar()
{
    mainToolBar_ = new QToolBar{ "Toolbar" };
    mainToolBar_->setObjectName("MainToolbar");

    QString testName{ "Test" };
    actionTest_ = new QAction{ testName, this };
    actionTest_->setIcon(QIcon{ ":/" + testName });
    actionTest_->setCheckable(true);
    actionTest_->setShortcut(QKeySequence{ "Ctrl+R" });
    mainToolBar_->addAction(actionTest_);
    connect(actionTest_, SIGNAL(triggered(bool)), this, SLOT(toggleTestDrive(bool)));

    mainToolBar_->addSeparator();

    for (bool undo: { true, false })
    {
        QString  actionName{ QString{ (undo ? "Un" : "Re") } + "do" };
        QAction* action{ new QAction{ actionName, this } };
        action->setObjectName(actionName + "action");
        action->setIcon(QIcon{ ":/" + actionName });
        action->setShortcut(QKeySequence{ "Ctrl+" + QString{ (undo ? "" : "Shift+") } + "Z" });

        if (undo)
        {
            actionUndo_ = action;
            connect(action, SIGNAL(triggered(bool)), this, SLOT(undo()));
        }
        else
        {
            actionRedo_ = action;
            connect(action, SIGNAL(triggered(bool)), this, SLOT(redo()));
        }

        mainToolBar_->addAction(action);
    }

    updateUndoRedoAvailable();

    addToolBar(mainToolBar_);
}

void Qocoon::createScene()
{
    Scene* scene{ new Scene{ context_ } };
    scene->SetUpdateEnabled(false);
    scene->CreateComponent<Octree>();
    scene->CreateComponent<PhysicsWorld>();
    scene->CreateComponent<DebugRenderer>();
    view3d_->setScene(scene);

    Node* sunNode{ scene->CreateChild("Sun") };
    Light* sun{ sunNode->CreateComponent<Light>() };
    sunNode->SetPosition(5 * Vector3{ .55f, 2.3f, 1.7f });
    sunNode->LookAt(Vector3::ZERO);
    sun->SetLightType(LIGHT_DIRECTIONAL);
    sun->SetRange(100.f);
    sun->SetCastShadows(true);
    sun->SetShadowIntensity(.3f);
    sun->SetShadowBias({ .0000125f, .5f });

    Node* garageNode{ scene->CreateChild("Garage") };
    garage_ = garageNode->CreateComponent<Garage>();
    garageNodeId_ = garageNode->GetID();
}

void Qocoon::toggleTestDrive(bool checked)
{
    if (actionTest_->isChecked() != checked)
        actionTest_->setChecked(checked);

    pressedKeys_.clear();
    view3d_->setContinuousUpdate(checked);
    updateUndoRedoAvailable();

    if (checked)
    {
        actionTest_->setIcon(QIcon{ ":/TestIn" });
    }
    else
    {
        actionTest_->setIcon(QIcon{ ":/Test" });
        garage_ = view3d_->scene()->GetNode(garageNodeId_)->GetComponent<Garage>();
        skeletonWidget_->updateModel(Vehicle::GetActive()->GetAnimatedModel());
    }
}

QMenu* Qocoon::createMenuBar()
{
    Weaver* weaver{ GetSubsystem<Weaver>() };

    QMenu* fileMenu{ new QMenu{ "File" } };

    fileMenu->addAction(QIcon{ ":/New" },  "New Project...", this, SLOT(newProject()), QKeySequence{ "Ctrl+N" });
    fileMenu->addAction(QIcon{ ":/Open" }, "Open...",        this, SLOT(open()),       QKeySequence{ "Ctrl+O" });
    recentMenu_ = fileMenu->addMenu("Recent");
    connect(this, SIGNAL(recentFilesChanged()), SLOT(updateRecentFilesMenu()));
    updateRecentFilesMenu();

    fileMenu->addSeparator();
    projectDependentActions_.push_back(fileMenu->addAction(QIcon{}, "Close",
                                                           this, SLOT(closeProject())));
    projectDependentActions_.push_back(fileMenu->addAction(QIcon{ ":/Save" }, "Save Project...",
                                                           this, SLOT(savePrefab()), QKeySequence{ "Ctrl+S" }));

    fileMenu->addSeparator();
    projectDependentActions_.push_back(fileMenu->addAction(QIcon{ ":/Project" }, "ProjectSettings...",
                                                           this, SLOT(editProject())));

    fileMenu->addSeparator();
    fileMenu->addAction(QIcon{ ":/Quit" }, "Exit", weaver, SLOT(quit()), QKeySequence{ "Ctrl+Q" });
    menuBar()->addMenu(fileMenu);

    QMenu* viewMenu{ new QMenu{ "View" } };
    viewMenu->addAction(actionFullView_);
    menuBar()->addMenu(viewMenu);

    QMenu* helpMenu{ new QMenu{ "Help" } };
    helpMenu->addAction(QIcon{ ":/Icon" }, tr("About %1...").arg(Weaver::applicationDisplayName()), this, SLOT(about()));
    menuBar()->addMenu(helpMenu);

    updateAvailableActions();

    return fileMenu;
}

void Qocoon::updateAvailableActions()
{
    for (QAction* action: projectDependentActions_)
        action->setEnabled(project_ != nullptr);
}

void Qocoon::updateRecentFilesMenu()
{
    QSettings settings{};
    QStringList recentFiles{( settings.value("recentfiles").toStringList() )};

    recentMenu_->clear();

    for (const QString& recent: recentFiles)
    {
        const QString displayName{ recent };
        recentMenu_->addAction(displayName, this, SLOT(openRecent()))->setObjectName(recent);
    }
}

void Qocoon::updateRecent(const QString& latest)
{
    if (latest.isEmpty())
        return;

    QSettings settings{};
    QStringList recentFiles{( settings.value("recentfiles").toStringList() )};

    for (const QString& recent: recentFiles)
    {
        if (!QFileInfo(recent).exists() || latest == recent )
            recentFiles.removeOne(recent);
    }

    recentFiles.push_front(latest);

    while (recentFiles.size() > 10)
        recentFiles.pop_back();

    settings.setValue("recentfiles", recentFiles);

    emit recentFilesChanged();
}

void Qocoon::open(String filename) /// Open project, model or prefab
{
    if (filename.IsEmpty()) // Pick through a file dialog
    {
        QString startPath{ /*project_ ? toQString(projectLocation())
                                    :*/ QStandardPaths::writableLocation(QStandardPaths::HomeLocation) };

        filename = toString(QFileDialog::getOpenFileName(nullptr, tr("Open File"), startPath, "*.lkp *xml *.mdl"));
    }

    if (filename.IsEmpty()) // Cancelled
        return;

    bool success{ false };

    if (filename.EndsWith(".lkp"))
    {
        auto p{ std::make_shared<Project>() };

        if (p->read(filename.CString()))
            success = openProject(p);
    }
    else if (filename.EndsWith(".xml"))
    {
        //Open prefab
        //Find/Create project
    }
    else if (filename.EndsWith(".mdl"))
    {
        success = loadModel(filename);
        //Open model
        //Find/Create project
        //Check for prefabs
    }

    if (success)
        updateRecent(DQ(filename));
}

bool Qocoon::openProject(std::shared_ptr<Project> project)
{
    if (project_ != project)
    {
        project_ = project;

        if (project != nullptr)
        {
            for (auto f: project_->info_.resourceFolders_)
            {
                GetSubsystem<ResourceCache>()->AddResourceDir(
                            AddTrailingSlash({ project_->info_.location_.data() }) + f.data());
            }

            statusBar()->showMessage({ ("Opened project " + project_->info_.displayName_).data() });
        }

        emit currentProjectChanged();

        return true;
    }

    return false;
}

bool Qocoon::loadModel(const String& filename)
{
    if (actionTest_->isChecked())
        toggleTestDrive(false);

    Vehicle* newVehicle{ createVehicle() };
    if (!newVehicle)
        return false;

    if (!filename.IsEmpty())
    {
        Model* model{ CACHE(Model)(filename) };

        if (model)
        {
            newVehicle->SetModel(model);
            newVehicle->SetName(GetFileName(filename));
        }
        else
        {
            Log::Write(LOG_ERROR, "Failed to load model: " + filename);
            return false;
        }
    }
    else
    {
        newVehicle->SetModel(nullptr);
    }

    garage_->SetSize(newVehicle->GetScale(), vehicles_.size());
    const float scale{ garage_->GetScale() };

    for (unsigned v{ 0 }; v < vehicles_.size() - 1; ++v)
    {
        Vehicle* vehicle{ vehicles_.at(v) };
        const Vector3 pos{ vehicle->GetNode()->GetWorldPosition().ProjectOntoPlane(Vector3::RIGHT) };
        const Vector3 shift{ Vector3::RIGHT * scale * (vehicles_.size() - v - 1u) };
        vehicle->GetNode()->SetPosition(pos + shift);
        vehicle->ResetWheels();
    }

    statusBar()->showMessage(toQString("Opened: " + filename));

    newVehicle->SetActive();
    view3d_->jib()->reset();
    emit activeModelChanged(newVehicle->GetAnimatedModel());
    view3d_->updateView();

    return true;
}

bool Qocoon::isRunning() const { return actionTest_->isChecked(); }

void Qocoon::openRecent()
{
    open(QD(sender()->objectName()));
}

void Qocoon::keyPressEvent(QKeyEvent* event)
{
    if (!isRunning())
        return;

    const Qt::Key k{ static_cast<Qt::Key>(event->key()) };

    if (k == Qt::Key_Escape)
    {
        actionTest_->trigger();
        return;
    }

    QSet<Qt::Key> relevantKeys{
        Qt::Key_Up,     Qt::Key_W,
        Qt::Key_Left,   Qt::Key_A,
        Qt::Key_Right,  Qt::Key_D,
        Qt::Key_Down,   Qt::Key_S,
        Qt::Key_Space,  Qt::Key_Shift
    };

    if (relevantKeys.contains(k))
        pressedKeys_.insert(k);
}

void Qocoon::keyReleaseEvent(QKeyEvent* event)
{
    if (!isRunning())
        return;


    const Qt::Key k{ static_cast<Qt::Key>(event->key()) };

    QSet<Qt::Key> relevantKeys{
        Qt::Key_Up,     Qt::Key_W,
        Qt::Key_Left,   Qt::Key_A,
        Qt::Key_Right,  Qt::Key_D,
        Qt::Key_Down,   Qt::Key_S,
        Qt::Key_Space,  Qt::Key_Shift
    };

    if (relevantKeys.contains(k))
        pressedKeys_.remove(k);
}

Vehicle* Qocoon::createVehicle()
{
    if (!view3d_->scene())
        return nullptr;

    SharedPtr<Vehicle> vehicle{ context_->CreateObject<Vehicle>() };
    vehicle->Init(view3d_->scene());
    vehicles_.push_back(vehicle);

    return vehicle.Get();
}

bool Qocoon::savePrefab()
{
//    if (project_)
//        return project_->Save();
//    else
        return false;
}

void Qocoon::newProject()
{
    ProjectWizard* wizard{ new ProjectWizard{ this } };

    if (wizard->result() == QDialog::Accepted)
    {
        auto p{ wizard->project().lock() };

        if (project_ != p)
        {
            p->save();
            openProject(p);
        }
    }

    delete wizard;
}

void Qocoon::closeProject() //\\\ Should check for modified files
{
//    if (!project_)
//        return;

//    hololith_->setCurrentIndex(0);

//    project_->remove();
//    project_ = nullptr;

//    emit currentProjectChanged();
}

void Qocoon::editProject()
{
    ProjectWizard* wizard{ new ProjectWizard{ this, project_.get() } };
    auto p{ wizard->project().lock() };

    if (wizard->result() == QDialog::Accepted)
        openProject(p);

    delete wizard;
}

void Qocoon::toggleFullView(bool toggled)
{
    QSettings settings{};

    if (toggled)
    {
        settings.setValue("geometry", saveGeometry());
        settings.setValue("state", saveState());

        for (QDockWidget* dock: findChildren<QDockWidget*>())
        {
            if (dock->isVisible())
                dock->setVisible(false);
        }

        statusBar()->setVisible(false);
        menuBar()->setVisible(false);
        mainToolBar_->setVisible(false);

        if (!isFullScreen())
            showFullScreen();

        actionFullView_->setShortcuts({ QKeySequence{ "F11" }, QKeySequence{ "Esc" } });
    }
    else
    {
        statusBar()->setVisible(true);
        menuBar()->setVisible(true);

        if (isFullScreen())
            showMaximized();

        restoreGeometry(settings.value("geometry").toByteArray());
        restoreState(settings.value("state").toByteArray());

        actionFullView_->setShortcuts({ QKeySequence{ "F11" } });
    }
}

void Qocoon::resizeEvent(QResizeEvent* event)
{
    if (static_cast<double>(event->size().width()) / event->size().height() > 1.0)
        setCorner(Qt::BottomRightCorner, Qt::RightDockWidgetArea);
    else
        setCorner(Qt::BottomRightCorner, Qt::BottomDockWidgetArea);
}

void Qocoon::about()
{
    QString aboutText{ tr("<p>Copyleft 🄯 2023 <a href=\"https://luckey.games\">LucKey Productions</a></b>"
                          "<p>You may use and redistribute this software under the terms "
                          "of the<br><a href=\"https://www.gnu.org/licenses/gpl.html\">"
                          "GNU General Public License Version 3</a>.</p>") };

    QDialog* aboutBox{ new QDialog{ this } };

    aboutBox->setWindowTitle("About " + Weaver::applicationDisplayName());
    QVBoxLayout* aboutLayout{ new QVBoxLayout{} };
    aboutLayout->setContentsMargins(0, 8, 0, 4);

    QPushButton* GarashButton{ new QPushButton{} };
    QPixmap moped{ ":/About" };
    GarashButton->setIcon(moped);
    GarashButton->setFlat(true);
    GarashButton->setMinimumSize(moped.width() * 02, moped.height());
    GarashButton->setIconSize(moped.size());
    GarashButton->setToolTip("https://gitlab.com/luckeyproductions/tools/garash");
    GarashButton->setCursor(Qt::CursorShape::PointingHandCursor);
    aboutLayout->addWidget(GarashButton);
    aboutLayout->setAlignment(GarashButton, Qt::AlignHCenter);
    aboutLayout->setContentsMargins(16, 23, 16, 0);

    connect(GarashButton, SIGNAL(clicked(bool)), this, SLOT(openUrl()));

    QLabel* aboutLabel{ new QLabel{ aboutText } };
    aboutLabel->setWordWrap(true);
    aboutLabel->setAlignment(Qt::AlignJustify);
    QVBoxLayout* labelLayout{ new QVBoxLayout{} };
    labelLayout->addWidget(aboutLabel);
    aboutLayout->addLayout(labelLayout);

    QDialogButtonBox* buttonBox{ new QDialogButtonBox{ QDialogButtonBox::Ok, aboutBox } };
    connect(buttonBox, SIGNAL(accepted()), aboutBox, SLOT(accept()));
    aboutLayout->addWidget(buttonBox);

    aboutBox->setLayout(aboutLayout);
    aboutBox->resize(aboutBox->minimumSize());
    aboutBox->exec();
}
void Qocoon::openUrl()
{
    QDesktopServices::openUrl(QUrl{ qobject_cast<QPushButton*>(sender())->toolTip() });
}

void Qocoon::updateUndoRedoAvailable()
{
    actionUndo_->setEnabled(!isRunning() && undoView_->stack()->canUndo());
    actionRedo_->setEnabled(!isRunning() && undoView_->stack()->canRedo());
}

void Qocoon::undo()
{
    undoView_->stack()->undo();
}

void Qocoon::redo()
{
    undoView_->stack()->redo();
}
