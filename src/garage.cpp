/* Garash
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "garage.h"

Garage::Garage(Context* context): Component(context),
    scale_{ 0.f },
    wallNode_{ nullptr },
    pillars_{}
{
}

void Garage::SetSize(float scale, int spaces)
{
    if (scale > scale_)
        scale_ = scale;

    node_->SetScale(scale_ * Vector3{ spaces + 2.f, 1.f, 5.f });
    node_->SetPosition(Vector3::UP * scale_ * .5f + Vector3::RIGHT * (spaces - 1) * scale_ * .5f);
}

void Garage::OnNodeSet(Node* node)
{
    if (!node)
        return;

    wallNode_ = node_->CreateChild("Walls");
    StaticModel* cell{ node_->CreateComponent<StaticModel>() };
    cell->SetModel(CACHE(Model)("Models/Cell.mdl"));

    node_->CreateComponent<RigidBody>();
    for (unsigned d{ 0u }; d < 6u; ++d)
    {
        Vector3 direction{};
        switch (d)
        {
        case 0u: direction = Vector3::RIGHT;   break;
        case 1u: direction = Vector3::LEFT;    break;
        case 2u: direction = Vector3::UP;      break;
        case 3u: direction = Vector3::DOWN;    break;
        default: continue;
        }

        CollisionShape* wall{ node_->CreateComponent<CollisionShape>() };

        if (d != 3u)
            wall->SetBox(Vector3::ONE - .5f * VectorAbs(direction), direction * .75f);
        else
            wall->SetStaticPlane(direction * .5f);
    }
}
