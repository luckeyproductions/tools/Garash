/* Garash
// Copyright (C) 2023 LucKey Productions (https://luckey.games)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#ifndef QOCOON_H
#define QOCOON_H

#include <memory>

#include <QSet>
#include <QMainWindow>
#include <QUndoView>

#include "project/project.h"
#include "skeletonwidget.h"
#include "bonewidget.h"
#include "resourcebrowser.h"

#include "drydockwidget.h"
#include "dry.h"

class View3D;
class Vehicle;
class Garage;

class Qocoon: public QMainWindow, public Object
{
    Q_OBJECT
    DRY_OBJECT(Qocoon, Object)

public:
    explicit Qocoon(Context* context);
    ~Qocoon();

    std::shared_ptr<Project> project() { return project_; }

    bool loadModel(const String& filename);

    QDockWidget* createDockWidget(QWidget* w, Qt::DockWidgetArea area)
    {
        QDockWidget* dockWidget{ new QDockWidget(w->objectName(), this) };
        dockWidget->setWidget(w);
        dockWidget->setObjectName(w->objectName() + "Dock");
        w->setParent(dockWidget);

        addDockWidget(area, dockWidget);
        return dockWidget;
    }

    bool isRunning() const;
    Vector3 getMove() const
    {
        const float x{ 1.f * ((pressedKeys_.contains(Qt::Key_D) || pressedKeys_.contains(Qt::Key_Right))
                            - (pressedKeys_.contains(Qt::Key_A) || pressedKeys_.contains(Qt::Key_Left))) };
        const float y{ 1.f * ((pressedKeys_.contains(Qt::Key_W) || pressedKeys_.contains(Qt::Key_Up))
                            - (pressedKeys_.contains(Qt::Key_S) || pressedKeys_.contains(Qt::Key_Down))) };
        const float z{ 1.f * (pressedKeys_.contains(Qt::Key_Shift) - pressedKeys_.contains(Qt::Key_Space)) };

        return { x, y, z };
    }

signals:
    void activeModelChanged(AnimatedModel* model);

    void resourceFoldersChanged();
    void recentFilesChanged();
    void currentProjectChanged();

public slots:
    void openRecent();

    void keyPressEvent(QKeyEvent* event) override;
    void keyReleaseEvent(QKeyEvent* event) override;

protected:
    void resizeEvent(QResizeEvent* event) override;

private slots:
    void newProject();
    void open(String filename = "");
    bool savePrefab();
    void editProject();
    void updateRecentFilesMenu();
    void updateAvailableActions();

    void toggleFullView(bool toggled);
    void about();
    void openUrl();
    void toggleTestDrive(bool checked);
    void undo();
    void redo();

    void closeProject();

private:
    QMenu* createMenuBar();
    void createToolBar();
    void loadSettings();
    void updateRecent(const QString& latest = "");
    void updateUndoRedoAvailable();

    void createScene();
    bool openProject(std::shared_ptr<Project> project);
    Vehicle* createVehicle();

    std::shared_ptr<Project> project_;
    unsigned garageNodeId_;
    Garage* garage_;
    std::vector<SharedPtr<Vehicle>> vehicles_;

    std::vector<QAction*> projectDependentActions_;
    QToolBar* mainToolBar_;
    QUndoView* undoView_;
    View3D* view3d_;
    SkeletonWidget* skeletonWidget_;
    BoneWidget* boneWidget_;
    ResourceBrowser* resourceBrowser_;


    QMenu* recentMenu_;
    QAction* actionTest_;
    QAction* actionUndo_;
    QAction* actionRedo_;
    QAction* actionFullView_;
    std::vector<QDockWidget*> hiddenDocks_;

    QSet<Qt::Key> pressedKeys_;
};

#endif // QOCOON_H
